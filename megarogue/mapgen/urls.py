# -*- coding: utf-8 -*-

from django.conf.urls import patterns, include, url


urlpatterns = patterns('megarogue.mapgen.views',
    url(r'^rev4', 'render'),
)