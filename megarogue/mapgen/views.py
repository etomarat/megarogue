# -*- coding: utf-8 -*-

import random, datetime as dt
import simplejson as json
import cPickle

from django.shortcuts import render_to_response
from django.http import HttpResponse

wall = '█'
floor = '░'
door = '.'

floor=0
wall=1
door=2
open_door=3

class World():
    '''
        Класс карты, при инициализации генерирует карту, используя текущее время как основу для генератора случайных чисел.

        map - карта
        width - ширина
        heigth - высота
    '''
    def __init__(self, seed=dt.datetime.now()):
        seed = str(seed)
        random.seed(seed)
        self.mapgen(seed)

    def mapgen(self, seed, x_min=50, x_max=100, y_min=3, y_max=3):
        width = random.randint(x_min, x_max)
        map = [[wall for i in range(width)] for i in range(random.randint(y_min, y_max))]
        X = random.randint(width/3, width-(width/3))
        x_max = x_min = X
        Y = 1
        map[Y][X] = floor
        for i in range(500):
            direction = random.randint(0, 6) # 0 = top, 1/2 = right, 3/4 = down, 5/6 = left
            if direction == 0:
                if Y-1: Y-=1
            elif direction in [3, 4]:
                Y+=1
                if Y >= len(map)-1: map.append([wall for i in range(width)])
            elif direction in [1, 2]:
                if X+1 < len(map): X+=1
                x_max = max(X, x_max)
            elif direction in [5, 6]:
                if X-1: X-=1
                x_min = min(X, x_min)
            map[Y][X] = floor
        map = [map[i][x_min-1:x_max+2] for i in range(len(map))]

        self.map = map
        self.width = x_max+3-x_min
        self.heigth = len(map)
        self.seed = seed
        
    def set_doors(self):
        door_n=0
        while door_n < 12:
            X = random.randint(1, len(self.map[0])-1)
            Y = random.randint(1, len(self.map)-1)
            if self.map[Y][X] == floor:
                if (self.map[Y-1][X] == self.map[Y+1][X] == wall and self.map[Y][X-1] ==  self.map[Y][X+1] == floor) \
                or (self.map[Y][X-1] == self.map[Y][X+1] == wall and self.map[Y-1][X] ==  self.map[Y+1][X] == floor):
                    self.map[Y][X] = door
                    door_n += 1


class Player():
    '''
    Класс игрока

    position - координаты игрока
    '''
    def set_position(self, map):
        while True:
            X = random.randint(1, len(map[0])-1)
            Y = random.randint(1, len(map)-1)
            if map[Y][X] == floor:
                self.position = [X, Y]
                break
        # return player_pos

def render(request):
    if request.method == 'GET' and 'seed' in request.GET:
        seed = request.GET.get('seed')
        file = open(str(seed)+'.map', 'r')
        world = cPickle.load(file)
        file.close()
    else:
        world = World(seed=dt.datetime.now())
        world.set_doors()
        seed = world.seed
    player = Player()
    player.set_position(world.map)
    
    file = open(str(seed)+".map", 'w')
    cPickle.dump(world, file)
    file.close()

    return render_to_response('mapgen2-g.html', {
        'world': world,
        'player': player,
        'seed': seed,
    })


def status(request):
    if request.method == 'GET':
        x = request.GET.get('x')
        y = request.GET.get('y')
        return HttpResponse(json.dumps([x, y]), mimetype='application/javascript')

