# -*- coding: utf-8 -*-

import random

from django.shortcuts import render_to_response


wall=1
floor=0
door=2

def f_concrete():
    width = random.randint(50,100)
    concrete = [[wall for i in range(width)] for i in range(random.randint(25,50))]
    dig(width, concrete)
    return doors(concrete)

def dig(width, concrete):
    X = random.randint(width/3, width-(width/3))
    Y = 1
    concrete[Y][X] = floor
    for i in range(500):
        direction = random.randint(0, 6) # 0 = top, 1/2 = right, 3/4 = down, 5/6 = left
        if direction == 0:
            if Y-1: Y-=1
        elif direction in [1, 2]:
            if X+1 < len(concrete): X+=1
        elif direction in [3, 4]:
            if Y+1 < len(concrete)-1:
                Y+=1
            else:
                concrete.append([wall for i in range(width)])
        elif direction in [5, 6]:
            if X-1: X-=1
        concrete[Y][X] = floor

def doors(map):
    door_n=0
    while door_n < 12:
        X = random.randint(1, len(map[0])-1)
        Y = random.randint(1, len(map)-1)
        if map[Y][X] == floor:
            if map[Y-1][X] == wall and map[Y+1][X] == wall and map[Y][X-1]==floor and map[Y][X+1]==floor:
                map[Y][X]=door
                door_n +=1
            elif map[Y][X-1]==wall and map[Y][X+1]==wall and map[Y-1][X] == floor and map[Y+1][X] == floor:
                map[Y][X]=door
                door_n +=1
    return map

def set_player(map):
    n = 0
    while n == 0:
        X = random.randint(1, len(map[0])-1)
        Y = random.randint(1, len(map)-1)
        if map[Y][X] == floor:
            player_pos = [X, Y]
            n += 1
    return player_pos


def render4(request):
    map = f_concrete()
    width = len(map[0])
    player = set_player(map)
    return render_to_response('mapgen2-g.html', {'map': map, 'width': width, 'player': player})