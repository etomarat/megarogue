# coding=utf8

from django.shortcuts import render_to_response
import random

wall='█'
floor='░'
door='.'

def f_concrete():
    width = random.randint(50,100)
    height = random.randint(25,50)
    concrete = []
    line = []
    for i in range(width):
        line.append(wall)
    for i in range(height):
        concrete.append(line[:])
    dig(width, height, concrete, line)
    return concrete

def dig(width, height, concrete, line):
    X = random.randint(0, width-1)
    Y = 1
    concrete[Y][X] = floor
    for i in range(500):
        direction = random.randint(0, 6) # 0 = top, 1/2 = right, 3/4 = down, 5/6 = left
        if direction == 0:
            if Y-1 < height and Y-1 > 0:
                Y-=1
                concrete[Y][X] = floor
        elif direction == 1 or direction == 2:
            if X+1 < height and X+1 > 0:
                X+=1
                concrete[Y][X] = floor
        elif direction == 3 or direction == 4:
            if Y+1 < height and Y+1 > 0:
                Y+=1
                concrete[Y][X] = floor
        elif direction == 5 or direction == 6:
            if X-1 < height and X-1 > 0:
                X-=1
                concrete[Y][X] = floor

def render3(request):
    map = f_concrete()
    return render_to_response('mapgen2.html', {'map': map,})