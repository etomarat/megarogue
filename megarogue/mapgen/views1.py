# coding=utf8

from django.shortcuts import render_to_response
import random

wall=u'█'
floor=u'░'


def oneroom(w,h):
    width = random.randint(3,20)
    height = random.randint(3,20)
    if w>0 and h>0:
        width = w
        height = h
    room = []
    s1 = ""
    s2 = ""
    for i in range(width):
        s1 += wall
    room.append(s1)
    for i in range(height):
        s2 = ""
        for t in range(width):
            s2 += floor
        s2 = wall + s2[1:-1] + wall
        room.append(s2)
    room.append(s1)
    return room

def bigroom():
    big_w = random.randint(50,100)
    big_h = random.randint(25,50)
    bigroom = oneroom(big_w, big_h)
    return bigroom

def mapslicer(map):
    X = random.randint(0, len(map[0])-1)
    Y = random.randint(0, len(map)-1)
    direction = random.randint(0, 3) # 0 = top-down, 1 = right-left, 2 = down-top, 3 = left-right
    if direction == 0:
        for i in range(1, len(map)-1):
            if map[i][X] != wall:
                map[i] = map[i][:X] + wall + map[i][X+1:]
            else:
                return
    
    elif direction == 1:
        for i in reversed(range(1, len(map[Y])-1)):
            if map[Y][i] != wall:
                map[Y] = map[Y][:i] + wall + map[Y][i+1:]
            else:
                return
    
    if direction == 2:
        for i in reversed(range(1, len(map)-1)):
            if map[i][X] != wall:    
                map[i] = map[i][:X] + wall + map[i][X+1:]
            else:
                return
            
    else:
        for i in range(1, len(map[Y])-1):
            if map[Y][i] != wall:
                map[Y] = map[Y][:i] + wall + map[Y][i+1:]
            else:
                return

def render(request):
    map = bigroom()
    for i in range(0, 5):
        mapslicer(map)
    check = '0'
    for i in map:
        print i
    
    return render_to_response('mapgen.html', {'room': map, 'check': check})
