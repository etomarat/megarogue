# coding=utf8

from django.shortcuts import render_to_response
import random

wall='█'
floor='░'

def f_concrete():
    width = random.randint(50,100)
    height = random.randint(25,50)
    concrete = []
    line = []
    for i in range(width):
        line.append(wall)
    for i in range(height):
        concrete.append(line[:])
    dig(width, height, concrete)
    return concrete

def dig(width, height, concrete):
    X = random.randint(0, width-1)
    Y = random.randint(0, height-1)
    concrete[Y][X] = floor
    for i in range(500):
        direction = random.randint(0, 3) # 0 = top, 1 = right, 2 = down, 3 = left
        if direction == 0:
            if Y-1 < height and Y-1 > 0:
                Y-=1
                concrete[Y][X] = floor
        elif direction == 1:
            if X+1 < height and X+1 > 0:
                X+=1
                concrete[Y][X] = floor
        elif direction == 2:
            if Y+1 < height and Y+1 > 0:
                Y+=1
                concrete[Y][X] = floor
        elif direction == 3:
            if X-1 < height and X-1 > 0:
                X-=1
                concrete[Y][X] = floor

def render2(request):
    map = f_concrete()
    return render_to_response('mapgen2.html', {'map': map,})