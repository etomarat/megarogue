# -*- coding: utf-8 -*-
############################################
#                                          #
#  Django settings for megarogue project.  #
#                                          #
############################################

import os.path

SITE_ROOT = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
path = lambda *args: os.path.join(SITE_ROOT, *args).replace('\\', '/')


DEBUG = bool(os.environ.get('DJANGO_DEBUG', False))
TEMPLATE_DEBUG = DEBUG


MEDIA_ROOT = path('media')
MEDIA_URL = '/media/'
LOCAL_MEDIA_ROOT = MEDIA_ROOT
LOCAL_MEDIA_URL = MEDIA_URL


THUMBNAIL_DIR = path( 'tmp')
THUMBNAIL_URL = MEDIA_URL + 'tmp/'


STATICFILES_STORAGE = 'django.contrib.staticfiles.storage.CachedStaticFilesStorage'
STATIC_ROOT = path('www', 'static')
STATIC_URL = '/static/'
STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
)
STATICFILES_DIRS = (
    path('megarogue', 'static'),
)


TEMPLATE_CONTEXT_PROCESSORS = (
    'django.contrib.auth.context_processors.auth',
    'django.core.context_processors.i18n',
    'django.core.context_processors.media',
    'django.core.context_processors.request',
    'django.core.context_processors.static',
)
TEMPLATE_LOADERS = (
    'django.template.loaders.filesystem.Loader',
    'django.template.loaders.app_directories.Loader',
)


ADMINS = ((
    os.environ.get('DJANGO_ADMIN_NAME', 'Admin'),
    os.environ.get('DJANGO_ADMIN_EMAIL', 'bugs@plan-b.ru'),
),)
MANAGERS = ((
    os.environ.get('DJANGO_MANAGER_NAME', 'Feedback'),
    os.environ.get('DJANGO_MANAGER_EMAIL', 'feedback@plan-b.ru')
),)
if DEBUG:
    MANAGERS = ADMINS


DATABASE_SQLITE = {
    'ENGINE': 'django.db.backends.sqlite3',
    'NAME': path('data.db')
}
DATABASES = {
    # 'default': DATABASE_SQLITE
}


TIME_ZONE = 'Europe/Moscow'

LANGUAGE_CODE = 'ru'


DATE_FORMAT = u'd F Y'
DATE_INPUT_FORMATS = (
    u'%d/%m/%Y',
)
TIME_FORMAT = u'H:i:s'
TIME_INPUT_FORMATS = (
    u'%H:%M:%S',
)
DATETIME_FORMAT = u'%s %s' % (DATE_FORMAT, TIME_FORMAT, )
DATETIME_INPUT_FORMATS = (
    u'%H:%M:%S %d/%m/%Y',
)

USE_I18N = True

APPEND_SLASH = True

SITE_ID = 1

gettext = lambda s: s
LOCALE_PATHS = (
    path('locale'),
)


MIDDLEWARE_CLASSES = (
    'django.middleware.csrf.CsrfViewMiddleware',
)


ROOT_URLCONF = 'megarogue.urls'

SECRET_KEY = 'io#v&amp;pv^7%g9l6tfnxv_ha904&amp;%tas--g17s*w6*3iw@lf4c_#'

SITE_NAME = 'megarogue'

KICK_YEAR = 2013

EMAIL_SUBJECT_PREFIX = u'[%s] ' % SITE_NAME

DEFAULT_FROM_EMAIL = 'noreply@plan-b.ru'

LANGUAGES = (
    ('ru', gettext(u'Русский')),
    # ('en', gettext(u'English')),
)

TEMPLATE_DIRS = (
    path('megarogue', 'templates'),
)

INSTALLED_APPS = (
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.sites',
    'django.contrib.staticfiles',
)

LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'filters': {
        'require_debug_false': {
            '()': 'django.utils.log.RequireDebugFalse'
        }
    },
    'handlers': {
        'mail_admins': {
            'level': 'ERROR',
            'filters': ['require_debug_false'],
            'class': 'django.utils.log.AdminEmailHandler'
        }
    },
    'loggers': {
        'django.request': {
            'handlers': ['mail_admins'],
            'level': 'ERROR',
            'propagate': True,
        },
    }
}