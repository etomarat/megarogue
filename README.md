mega-rogue
==========

Dependencies
============

    - python 2.7.3
    - django 1.5.1

Install
=======

	virtualenv .
	source bin/activate
	pip install -r reqs.txt

Use
===
	source bin/activate
	./manage.py runserver